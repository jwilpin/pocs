package test

import org.scalatest.{AsyncFlatSpec, EitherValues, FlatSpec, Matchers, WordSpec}

trait TestBase extends FlatSpec with Matchers with EitherValues
trait WordTestBase extends WordSpec with Matchers with EitherValues

trait AsyncTestBase extends AsyncFlatSpec with Matchers with EitherValues
