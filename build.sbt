import Dependencies._

lazy val commonSettings = Seq(
  version := "1.0.0-SNAPSHOT",
  scalaVersion := "2.12.9",
  resolvers += Resolver.sonatypeRepo("releases"),
  addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.10.3"),
  addCompilerPlugin("org.typelevel" % "kind-projector" % "0.10.3" cross CrossVersion.binary),
  libraryDependencies ++= (scalaBinaryVersion.value match {
    case "2.10" =>
      compilerPlugin("org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full) :: Nil
    case _ =>
      Nil
  })
)

lazy val root = (project in file("."))
  .settings(
    commonSettings,
    name := "POCs"
  )
  .aggregate(common, fp_patterns)

lazy val common = (project in file("Common"))
  .settings(
    commonSettings,
    name := "Common",
    libraryDependencies ++= commonDeps
  )

lazy val fp_patterns = (project in file("fp_patterns"))
  .settings(
    commonSettings,
    name := "FP_Patterns",
    scalacOptions += "-Ypartial-unification",
    libraryDependencies ++= fpPatternDeps
  )
  .dependsOn(common % "test->test;compile->compile")
