package functor

import container.{Just, Maybe, Nothing}
import functor.instances._
import functor.sintax._
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks
import test.WordTestBase

class FunctorTests extends WordTestBase with ScalaCheckPropertyChecks {

  "MaybeFunctor" should {
    "map a defined maybe correctly" in {
      val maybe: Maybe[String] = Just("hola mundo")
      val fx: String => Int = s => s.length
      maybe.map(fx) shouldBe Just(10)
    }
    "map an empty maybe correctly" in {
      val maybe: Maybe[String] = Nothing
      val fx: String => Int = s => s.length
      maybe.map(fx) shouldBe Nothing
    }
  }

  "ReaderFunctor" should {

    case class User(id: Int, name: String)
    case class Password(user: User, passwd: String)

    val pass1 = Password(User(1, "user 1"), "oooo")
    val pass2 = Password(User(2, "user 2"), "ups!")

    val userFn: Int => Option[User] = id => id match {
      case 1 => Option(pass1.user)
      case 2 => Option(pass2.user)
      case _ => None
    }

    val passFn: Option[User] => Option[Password] = user => user match {
      case Some(user) if user == pass1.user => Option(pass1)
      case Some(user) if user == pass2.user => Option(pass2)
      case _ => None
    }

    "compose reader fn with map fn" in {
      userFn.map(passFn).apply(1) shouldBe Option(pass1)
      userFn.map(passFn).apply(2) shouldBe Option(pass2)
      userFn.map(passFn).apply(3) shouldBe None
    }
  }

}
