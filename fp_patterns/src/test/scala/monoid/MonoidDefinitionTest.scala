package monoid

import monoid.MonoidDefinitionTest.RockPaperScissorsGame
import org.scalacheck.{Arbitrary, Gen}
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks
import test.TestBase

import RockPaperScissorsGame.{Rock, Paper, Scissors, Empty}

class MonoidDefinitionTest extends TestBase with ScalaCheckPropertyChecks {

  type T = RockPaperScissorsGame.Value

  val monoid: Monoid[T] = new Monoid[T] {

    // It is not associative...
    override def combine(v1: T, v2: T): T = List(v1, v2).sorted match {
      case List(Paper, Scissors) => Scissors
      case List(Rock, Paper)     => Paper
      case List(Rock, Scissors)  => Rock
      case List(Paper, Scissors) => Scissors
      case List(a, b) if a == b  => a
      case List(a, Empty)        => a
    }

    override def empty: T = Empty
  }

  implicit val gen: Arbitrary[T] = MonoidDefinitionTest.RockPaperScissorsGameGen

  implicit override val generatorDrivenConfig: PropertyCheckConfiguration =
    PropertyCheckConfig(minSuccessful = 1000, maxSize = 1000)

  "Monoid definition" should "define an identity for combine function" in {
    forAll { (value: T) =>
      monoid.combine(value, monoid.empty) shouldBe value
    }
  }

  // TODO: Fails because RockPaperScissorsGame monoid is not associative... define a valid monoid
  it should "define an associative combine function" ignore {
    forAll { (v1: T, v2: T, v3: T) =>
      monoid.combine(v1, monoid.combine(v2, v3)) shouldBe monoid.combine(monoid.combine(v1, v2), v3)
    }
  }

  it should "follow the game rules" in {
    monoid.combine(Scissors, Paper) shouldBe Scissors
    monoid.combine(Paper, Scissors) shouldBe Scissors

    monoid.combine(Scissors, Rock) shouldBe Rock
    monoid.combine(Rock, Scissors) shouldBe Rock

    monoid.combine(Paper, Rock) shouldBe Paper
    monoid.combine(Rock, Paper) shouldBe Paper

    monoid.combine(Rock, Empty) shouldBe Rock
    monoid.combine(Scissors, Empty) shouldBe Scissors
    monoid.combine(Paper, Empty) shouldBe Paper
  }

}

object MonoidDefinitionTest {

  object RockPaperScissorsGame extends Enumeration {
    val Rock, Paper, Scissors, Empty = Value
  }

  val RockPaperScissorsGameGen: Arbitrary[RockPaperScissorsGame.Value] =
    Arbitrary(Gen.oneOf(RockPaperScissorsGame.values.toSeq))

}
