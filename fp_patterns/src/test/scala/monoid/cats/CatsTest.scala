package monoid.cats

import cats.Monoid
import monoid.instances.MapIntMonoidTest.mapGen
import org.scalacheck.Arbitrary
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks
import test.WordTestBase

class CatsTest extends WordTestBase with ScalaCheckPropertyChecks {

  implicit override val generatorDrivenConfig: PropertyCheckConfiguration =
    PropertyCheckConfig(minSuccessful = 1000, maxSize = 1000)

  "cats.Monoid[Int]" should {

    import cats.instances.int._
    implicit val gen = Arbitrary.arbitrary[Int]

    "define a valid monoid for Int" in {
      Monoid[Int].empty shouldBe 0
      Monoid[Int].combineAll(List(1, 2, 3)) shouldBe 6
      Monoid[Int].combineAll(List()) shouldBe 0

      Monoid[Int].combine(1, Monoid[Int].empty) shouldBe 1
      Monoid[Int].combine(Monoid[Int].empty, 1) shouldBe 1

      Monoid[Int].combine(1, 5) shouldBe 6
    }

    behave like testIdentityForCombineFunction
    behave like testAssociativeCombineFunction
  }

  "cats.Monoid[String]" should {

    import cats.instances.string._
    implicit val gen = Arbitrary.arbitrary[String]

    "define a valid monoid for String" in {
      Monoid[String].empty shouldBe ""
      Monoid[String].combineAll(List("a", "b", "c")) shouldBe "abc"
      Monoid[String].combineAll(List()) shouldBe ""
    }

    behave like testIdentityForCombineFunction
    behave like testAssociativeCombineFunction
  }

  "cats.Monoid[Map[String, Int]]" should {

    import cats.instances.int._
    import cats.instances.map._
    implicit val gen: Arbitrary[Map[String, Int]] = Arbitrary(mapGen)

    "define a valid monoid" in {
      val map1 = Map("hello" -> 5, "world" -> 1)
      val map2 = Map("hello" -> 2, "cats" -> 3)

      val expected = Map("hello" -> 7, "world" -> 1, "cats" -> 3)

      Monoid[Map[String, Int]].combine(map1, map2) shouldBe expected

    }

    behave like testIdentityForCombineFunction[Map[String, Int]]
    behave like testAssociativeCombineFunction[Map[String, Int]]

  }

  "cats.Monoid[Map[String, Int]] for Monoid[Int] *" should {

    implicit val intMonoid: Monoid[Int] = Monoid.instance[Int](1, (v1, v2) => v1 * v2)

    import cats.instances.map._
    implicit val gen: Arbitrary[Map[String, Int]] = Arbitrary(mapGen)

    "define a valid monoid" in {
      val map1 = Map("hello" -> 5, "world" -> 1)
      val map2 = Map("hello" -> 2, "cats" -> 3)

      val expected = Map("hello" -> 10, "world" -> 1, "cats" -> 3)

      Monoid[Map[String, Int]].combine(map1, map2) shouldBe expected

    }

    behave like testIdentityForCombineFunction[Map[String, Int]]
    behave like testAssociativeCombineFunction[Map[String, Int]]

  }

  def testIdentityForCombineFunction[T](implicit monoid: Monoid[T], gen: Arbitrary[T]): Unit = {
    "define an identity for combine function" in {
      forAll { (value: T) =>
        monoid.combine(value, monoid.empty) shouldBe monoid.combine(monoid.empty, value)
      }
    }
  }

  def testAssociativeCombineFunction[T](implicit monoid: Monoid[T], gen: Arbitrary[T]): Unit = {
    "define an associative combine function" in {
      forAll { (v1: T, v2: T, v3: T) =>
        monoid.combine(v1, monoid.combine(v2, v3)) shouldBe monoid.combine(monoid.combine(v1, v2), v3)
      }
    }
  }

}
