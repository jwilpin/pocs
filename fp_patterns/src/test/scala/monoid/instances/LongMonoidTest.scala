package monoid.instances

import monoid.Monoid
import org.scalacheck.Arbitrary

class LongMonoidTest extends MonoidInstanceTestBase  {

  override type T = Long

  override val monoid: Monoid[T] = LongMonoid

  override implicit val gen: Arbitrary[T] = Arbitrary.arbLong

  "Int monoid" should "define a valid combine function" in {
    monoid.combine(1L, 2L) shouldBe 3L
  }

  it should behave like testIdentityForCombineFunction

  it should behave like testAssociativeCombineFunction

}
