package monoid.instances

import monoid.Monoid
import org.scalacheck.Arbitrary
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks
import test.TestBase

trait MonoidInstanceTestBase extends TestBase with ScalaCheckPropertyChecks with Instances {

  type T

  val monoid: Monoid[T]

  implicit val gen: Arbitrary[T]

  implicit override val generatorDrivenConfig: PropertyCheckConfiguration =
    PropertyCheckConfig(minSuccessful = 1000, maxSize = 1000)

  def testIdentityForCombineFunction: Unit = {
    it should "define an identity for combine function" in {
      forAll { (value: T) =>
        monoid.combine(value, monoid.empty) shouldBe monoid.combine(monoid.empty, value)
      }
    }
  }

  def testAssociativeCombineFunction: Unit = {
    it should "define an associative combine function" in {
      forAll { (v1: T, v2: T, v3: T) =>
        monoid.combine(v1, monoid.combine(v2, v3)) shouldBe monoid.combine(monoid.combine(v1, v2), v3)
      }
    }
  }

}
