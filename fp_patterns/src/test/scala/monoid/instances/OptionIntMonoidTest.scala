package monoid.instances

import monoid.Monoid
import org.scalacheck.Arbitrary

//import monoid.Implicits._

class OptionIntMonoidTest extends MonoidInstanceTestBase {

  override type T = Option[Int]

  implicit val intMonoid = IntMonoid
  override val monoid: Monoid[T] = OptionMonoid.apply[Int]

  override implicit val gen: Arbitrary[T] = Arbitrary.arbOption

  "Int monoid" should "define a valid combine function" in {
    monoid.combine(Some(1), Some(3)) shouldBe Some(4)
  }

  it should behave like testIdentityForCombineFunction

  it should behave like testAssociativeCombineFunction

}
