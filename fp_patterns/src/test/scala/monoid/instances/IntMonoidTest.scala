package monoid.instances

import monoid.Monoid
import org.scalacheck.Arbitrary

class IntMonoidTest extends MonoidInstanceTestBase  {

  override type T = Int

  override val monoid: Monoid[T] = IntMonoid

  override implicit val gen: Arbitrary[T] = Arbitrary.arbInt

  "Int monoid" should "define a valid combine function" in {
    monoid.combine(1, 2) shouldBe 3
  }

  it should behave like testIdentityForCombineFunction

  it should behave like testAssociativeCombineFunction

}
