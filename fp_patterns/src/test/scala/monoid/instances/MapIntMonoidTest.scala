package monoid.instances

import monoid.Monoid
import org.scalacheck.{Arbitrary, Gen}

import org.scalacheck.Arbitrary.arbitrary

class MapIntMonoidTest extends MonoidInstanceTestBase {

  override type T = Map[String, Int]

  implicit val intMonoid = IntMonoid
  override val monoid: Monoid[T] = MapMonoid.apply[String, Int]

  override implicit val gen: Arbitrary[T] = Arbitrary(MapIntMonoidTest.mapGen)

  "Int monoid" should "define a valid combine function" in {
    val map1 = Map("hello" -> 5, "world" -> 1)
    val map2 = Map("hello" -> 2, "cats" -> 3)

    monoid.combine(map1, map2) shouldBe Map("hello" -> 7, "world" -> 1, "cats" -> 3)
  }

  it should behave like testIdentityForCombineFunction

  it should behave like testAssociativeCombineFunction

}

object MapIntMonoidTest {

  val tupleGen: Gen[(String, Int)] = for {
    k <- arbitrary[String]
    v <- arbitrary[Int]
  } yield (k, v)

  val mapGen: Gen[Map[String, Int]] = for {
    values <- Gen.containerOf[List, (String, Int)](tupleGen)
  } yield {
    values.toMap
  }

}
