package monoid.instances

import monoid.Monoid
import org.scalacheck.Arbitrary

class StringMonoidTest extends MonoidInstanceTestBase  {

  override type T = String

  override val monoid: Monoid[T] = StringMonoid

  override implicit val gen: Arbitrary[T] = Arbitrary.arbString

  "Int monoid" should "define a valid combine function" in {
    monoid.combine("hola", "mundo") shouldBe "holamundo"
  }

  it should behave like testIdentityForCombineFunction

  it should behave like testAssociativeCombineFunction

}
