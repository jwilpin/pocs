package monoid.instances

import monoid.Monoid

class SeqMonoid[A] extends Monoid[Seq[A]] {
  def empty: Seq[A] = Seq.empty
  def combine(v1: Seq[A], v2: Seq[A]): Seq[A] = v1 ++ v2
}

object SeqMonoid {

  def apply[T: Monoid]: Monoid[Seq[T]] = new SeqMonoid[T]

}

