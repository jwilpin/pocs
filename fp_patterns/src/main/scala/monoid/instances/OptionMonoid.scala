package monoid.instances

import monoid.Monoid

class OptionMonoid[A](implicit monoid: Monoid[A]) extends Monoid[Option[A]] {
  def empty: Option[A] = None

  def combine(v1: Option[A], v2: Option[A]): Option[A] =
    v1 match {
      case None => v2
      case Some(a) =>
        v2 match {
          case None => v1
          case Some(b) => Some(monoid.combine(a, b))
        }
    }
}

object OptionMonoid {

  def apply[T: Monoid]: Monoid[Option[T]] = new OptionMonoid[T]

}
