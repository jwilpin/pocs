package monoid.instances

import monoid.Monoid
import monoid.sintax._

class MapMonoid[K, V](implicit monoid: Monoid[V]) extends Monoid[Map[K, V]] {

  def empty: Map[K, V] = Map.empty

  def combine(v1: Map[K, V], v2: Map[K, V]): Map[K, V] = {
    mergeMap(v1, v2)
  }

  def mergeMap[K, V: Monoid](lhs: Map[K, V], rhs: Map[K, V]): Map[K, V] =
    lhs.foldLeft(rhs) {
      case (acc, (k, v)) =>
        acc.updated(k, optionCombine(v, acc.get(k)))
    }

  def optionCombine[A: Monoid](a: A, opt: Option[A]): A =
    opt.map(a |+| _).getOrElse(a)

}


object MapMonoid {

  def apply[K, V: Monoid]: Monoid[Map[K, V]] = new MapMonoid[K, V]

}
