package monoid.instances

import monoid.Monoid

class ListMonoid[A] extends Monoid[List[A]] {
  def empty: List[A] = Nil
  def combine(v1: List[A], v2: List[A]): List[A] = v1 ::: v2
}

object ListMonoid {

  def apply[T: Monoid]: Monoid[List[T]] = new ListMonoid[T]

}