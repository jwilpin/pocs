package monoid.instances

import monoid.Monoid

trait Instances {

  implicit val IntMonoid = Monoid.instance[Int](identity = 0, combineFn = (v1, v2) => v1 + v2)

  implicit val LongMonoid = Monoid.instance[Long](identity = 0L, combineFn = (v1, v2) => v1 + v2)

  implicit val StringMonoid = Monoid.instance[String](identity = "", combineFn = (v1, v2) => v1.concat(v2))

  implicit val AndBooleanMonoid = Monoid.instance[Boolean](identity = true, combineFn = (v1, v2) => v1 && v2)

  implicit val OrBooleanMonoid = Monoid.instance[Boolean](identity = false, combineFn = (v1, v2) => v1 || v2)

}
