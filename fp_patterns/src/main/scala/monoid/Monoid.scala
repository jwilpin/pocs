package monoid

trait Monoid[T] {
  def combine(v1: T, v2: T): T
  def empty: T
}

object Monoid {

  final def apply[A](implicit ev: Monoid[A]): Monoid[A] = ev

  def instance[T](identity: T, combineFn: (T, T) => T): Monoid[T] = {
    new Monoid[T] {
      override def combine(v1: T, v2: T): T = combineFn(v1, v2)

      override def empty: T = identity
    }
  }

}