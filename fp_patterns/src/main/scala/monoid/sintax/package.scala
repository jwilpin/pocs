package monoid

package object sintax {

  implicit class MonoidOps[T](value: T)(implicit monoid: Monoid[T]) extends AnyRef {
    def identity: T = monoid.empty
    def append(v2: T): T = monoid.combine(value, v2)
    def |+|(v2: T): T = monoid.combine(value, v2)
  }
}
