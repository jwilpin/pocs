package functor

package object sintax {
  case class FunctorOps[F[_], A](fa: F[A]) {

    def map[B](fx: A => B)(implicit functor: Functor[F]): F[B] =
      functor.map(fa)(fx)
  }
  implicit def toFunctorOps[F[_], A](fa: F[A]): FunctorOps[F, A] = FunctorOps(fa)
}
