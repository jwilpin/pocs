package functor

import container.Maybe

import scala.concurrent.{ExecutionContext, Future}

package object instances {
  implicit val listFunctor: Functor[List] = new ListFunctor
  implicit val optionFunctor: Functor[Option] = new OptionFunctor
  implicit val maybeFunctor: Functor[Maybe] = new MaybeFunctor
  implicit def reader1Functor[R]: Functor[R => ?] = new ReaderFunctor[R]
  implicit def futureFunctor(implicit ex: ExecutionContext): Functor[Future] = new FutureFunctor
}
