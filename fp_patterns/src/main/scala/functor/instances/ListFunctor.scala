package functor.instances

import functor.Functor
import scala.language.higherKinds

class ListFunctor extends Functor[List]{
  override def map[A, B](fa: List[A])(fx: A => B): List[B] = fa map fx
}