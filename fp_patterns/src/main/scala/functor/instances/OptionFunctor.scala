package functor.instances

import functor.Functor

class OptionFunctor extends Functor[Option] {
  override def map[A, B](fa: Option[A])(fx: A => B): Option[B] = fa match {
    case None    => None
    case Some(a) => Some(fx(a))
  }
}
