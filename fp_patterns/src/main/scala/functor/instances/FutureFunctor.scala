package functor.instances

import functor.Functor

import scala.concurrent.{ExecutionContext, Future}

class FutureFunctor(implicit ex: ExecutionContext) extends Functor[Future] {
  override def map[A, B](fa: Future[A])(fx: A => B): Future[B] = fa map fx
}
