package functor.instances

import container.{Just, Maybe, Nothing}
import functor.Functor

class MaybeFunctor extends Functor[Maybe] {
  override def map[A, B](fa: Maybe[A])(fx: A => B): Maybe[B] = fa match {
    case Nothing => Nothing
    case Just(a) => Just(fx(a))
  }
}
