package functor.instances

import functor.Functor

// Uses https://github.com/typelevel/kind-projector
// see also https://gist.github.com/fsarradin/5806bc6324f44161304e
class ReaderFunctor[R] extends Functor[R => ?] {
  override def map[A, B](fa: R => A)(fx: A => B): R => B = fa andThen fx
}
