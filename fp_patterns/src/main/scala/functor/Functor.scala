package functor

import scala.language.higherKinds

trait Functor[F[_]] {
  def map[A, B](fa: F[A])(fx: A => B): F[B]
}
