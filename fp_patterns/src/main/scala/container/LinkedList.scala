package container

sealed trait LinkedList[+E]
case object Nil extends LinkedList[Nothing]
case class Cons[E](h: E, t: List[E]) extends LinkedList[E]
