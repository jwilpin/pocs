package container

sealed trait Maybe[+A]
case object Nothing extends Maybe[Nothing]
case class  Just[A](a: A) extends Maybe[A]
