import sbt._

object Dependencies {
  val scalatest = "org.scalatest" %% "scalatest" % "3.0.8"
  val scalamock = "org.scalamock" %% "scalamock-scalatest-support" % "3.6.0"
  val scalacheck = "org.scalacheck" %% "scalacheck" % "1.14.0"

  val catsDeps = Seq(
    "org.typelevel" %% "cats-core" % "2.0.0-RC1"
  )

  // Sub-project specific dependencies
  lazy val commonDeps = Seq(
    scalatest,
    scalamock,
    scalacheck
  )

  lazy val fpPatternDeps = commonDeps ++ catsDeps
  
}
